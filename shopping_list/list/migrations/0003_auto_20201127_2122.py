# Generated by Django 3.1.3 on 2020-11-27 21:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('list', '0002_auto_20201127_2121'),
    ]

    operations = [
        migrations.RenameField(
            model_name='items',
            old_name='list_id',
            new_name='list',
        ),
    ]
