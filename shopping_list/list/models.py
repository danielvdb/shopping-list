import datetime

from django.db import models


class List(models.Model):
    name = models.CharField(max_length=200)
    date_insert = models.DateTimeField('date insert')
    date_update = models.DateTimeField('date update')

    def __str__(self):
        return self.name

    def was_published_recently(self):
        return self.date_insert >= timezone.now() - datetime.timedelta(days=1)


class Item(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
