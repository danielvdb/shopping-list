from django.urls import path

from . import views

app_name = 'list'
urlpatterns = [
    # ex: /list/
    path('', views.index, name='index'),
    # ex: /list/items/5/
    path('items/<int:list_id>/', views.items, name='items'),
    # ex: /items/5/add/
    path('items/<int:list_id>/add/', views.add_item, name='add_item'),
    # ex: /items/5/remove/2/
    path('items/<int:list_id>/delete/<int:item_id>', views.delete_item, name='delete_item'),
    # ex: /items/5/edit/2/
    path('items/<int:list_id>/edit/<int:item_id>', views.edit_item, name='edit_item'),
]