from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .models import List


def index(request):
    shopping_lists = List.objects.order_by('-date_insert')[:5]
    context = {
        'shopping_lists': shopping_lists,
    }
    return render(request, 'list/index.html', context)


def items(request, list_id):
    shopping_list = get_object_or_404(List, pk=list_id)
    return render(request, 'list/items.html', {'shopping_list': shopping_list})


def add_item(request, list_id):
    return HttpResponse("Add a item to list %s." % list_id)


def delete_item(request, item_id, list_id):
    response = HttpResponse()
    response.write('<p>Delete item</p>')
    response.write('<p>List %s</p>' % list_id)
    response.write('<p>Item %s</p>' % item_id)
    return response


def edit_item(request, item_id, list_id):
    response = HttpResponse()
    response.write('<p>Edit item</p>')
    response.write('<p>List %s</p>' % list_id)
    response.write('<p>Item %s</p>' % item_id)
    return response
